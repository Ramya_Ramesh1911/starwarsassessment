import { Component,  OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StarWarsApiService } from '../star-wars-api.service';


@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {

 
 
 filmEpisodePopup!:any;
 
 

  constructor(private dialogRef: MatDialog, private starWarsService: StarWarsApiService ) { }
  
  

  closeDialog() //This method is called when user clicks on "Save" button
  {
    this.dialogRef.closeAll();
  }

  ngOnInit(): void {
    
   switch(this.starWarsService.id)
   {
    case 1: 
    this.starWarsService.getFilmsEpisodeOne().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;

    case 2: 
    this.starWarsService.getFilmsEpisodeTwo().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;

    case 3: 
    this.starWarsService.getFilmsEpisodeThree().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;

    case 4:
    this.starWarsService.getFilmsEpisodeFour().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;

    case 5: 
    this.starWarsService.getFilmsEpisodeFive().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;

    case 6:
    this.starWarsService.getFilmsEpisodeSix().subscribe(data=>{
    this.filmEpisodePopup=data; })
    break;
   }
        
        }
   
    
  }

    







