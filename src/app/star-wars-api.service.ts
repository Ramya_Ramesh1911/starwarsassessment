import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Films } from 'src/star-wars-films';


@Injectable({
  providedIn: 'root'
})

//Service to consume API data
export class StarWarsApiService {
  public id!: number;

  constructor(private httpClient: HttpClient) {}
  getFilmsEpisodeOne(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/4/";
    return this.httpClient.get<Films>(baseUrl);
  }
  getFilmsEpisodeTwo(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/5/";
    return this.httpClient.get<Films>(baseUrl);
  }
  getFilmsEpisodeThree(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/6/";
    return this.httpClient.get<Films>(baseUrl);
  }
  getFilmsEpisodeFour(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/1/";
    return this.httpClient.get<Films>(baseUrl);
  }
  getFilmsEpisodeFive(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/2/";
    return this.httpClient.get<Films>(baseUrl);
  }
  getFilmsEpisodeSix(): Observable<Films>{
    const baseUrl="https://swapi.dev/api/films/3/";
    return this.httpClient.get<Films>(baseUrl);
  }
  
 
}
