import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PopUpComponent } from '../pop-up/pop-up.component';

import { StarWarsApiService } from '../star-wars-api.service';
import { Films } from 'src/star-wars-films';



@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
  
})
export class CardsComponent implements OnInit {
  [x: string]: any;
  
 
    
  
  constructor( private dialogRef: MatDialog,private starWarsService: StarWarsApiService) { }

  //Below variables are defined to store the JSON object of the films which are of Films type.
  //Films is an interface which is defined in star-wars-films.ts
  //StarWarsApiService is the service where methods are defined to consume API data and these methods are defined in star-wars-api-service.ts
  filmEpisodeOne!: Films;
  filmEpisodeTwo!: Films;
  filmEpisodeThree!: Films;
  filmEpisodeFour!: Films;
  filmEpisodeFive!: Films;
  filmEpisodeSix!: Films;
  
 //OpenDialogOne() to OpenDialogSix() -> Functions called when the buttons of the particular episodes are clicked
  openDialogOne()
  {
     this.starWarsService.id=1;
    this.dialogRef.open(PopUpComponent);
   
   
  }
  openDialogTwo()
  {
    this.starWarsService.id=2;
    this.dialogRef.open(PopUpComponent);
    
  }
  openDialogThree()
  {
    this.starWarsService.id=3;
    this.dialogRef.open(PopUpComponent);
    
  }
  openDialogFour()
  {
    this.starWarsService.id=4;
    this.dialogRef.open(PopUpComponent);
    
  }
  openDialogFive()
  {
    this.starWarsService.id=5;
    
    this.dialogRef.open(PopUpComponent);
    
  }
  openDialogSix()
  {
    this.starWarsService.id=6;
    
    this.dialogRef.open(PopUpComponent);
    
  }
//getFilmsEpisodeOne() to getFilmsEpisodeSix() -> Calling the methods in the star-wars-api-service to get the JSON object of the film
  ngOnInit(): void {
    this.starWarsService.getFilmsEpisodeOne().subscribe(data=>{
      this.filmEpisodeOne=data;
    
    })
    
    this.starWarsService.getFilmsEpisodeTwo().subscribe(data=>{
      this.filmEpisodeTwo=data;
    })
    this.starWarsService.getFilmsEpisodeThree().subscribe(data=>{
      this.filmEpisodeThree=data;
    })
    this.starWarsService.getFilmsEpisodeFour().subscribe(data=>{
      this.filmEpisodeFour=data;
    })
    this.starWarsService.getFilmsEpisodeFive().subscribe(data=>{
      this.filmEpisodeFive=data;
    })
    this.starWarsService.getFilmsEpisodeSix().subscribe(data=>{
      this.filmEpisodeSix=data;
    })
    
    

  }
}
