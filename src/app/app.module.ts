import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { CardsComponent } from './cards/cards.component';
import { PopUpComponent } from './pop-up/pop-up.component';
import { HttpClientModule } from '@angular/common/http';
import { StarWarsApiService } from './star-wars-api.service';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CardsComponent,
    PopUpComponent
    
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    HttpClientModule

    
  
  ],
  providers: [StarWarsApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
